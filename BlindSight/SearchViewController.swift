//
//  SearchViewController.swift
//  BlindSight
//
//  Created by Lucas Teixeira Carletti on 14/08/2016.
//  Copyright © 2016 Lucas Mac-Pro. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import MapKit

protocol SelectLocation {
    func selectedLocation(_ placemark:MKMapItem?)
}
class SearchViewController : UIViewController, UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,CLLocationManagerDelegate,MKMapViewDelegate
{
    //MARK : - Outlets
    
    @IBOutlet var tableView: UITableView!{
        didSet{
            tableView.delegate = self
            tableView.dataSource = self
        }
    }
    
    @IBOutlet var searchBar: UISearchBar!
    
    //MARK : - Variables
    
    var dataSource : [MKMapItem]?{
        didSet{
            if let table = tableView{
                table.reloadData()
            }
        }
    }
    var mapCopy : MKMapItem!
    var search : MKLocalSearch!
    
    var selectedDelegate : SelectLocation?
    
    //MARK : - SearchBar Delegates
    
    func requestRoute(_ location :String){
        let request = MKLocalSearchRequest()
        request.naturalLanguageQuery = location
        if let sear = search{
            sear.cancel()
        }
        search = MKLocalSearch(request: request)
        
        search.start(){response,error in
            
            if error != nil {
                print("Error occured in search: \(error!.localizedDescription)")
            } else if response!.mapItems.count == 0 {
                print("No matches found")
            } else {
                if let data = response?.mapItems{
                    self.dataSource = data
                }
                print("Matches found")
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
    func drawOnMap(){
    
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(polyline: overlay as! MKPolyline)
        renderer.strokeColor = UIColor.blue
        return renderer
    }


    //MARK : - SearchBar Delegates
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        requestRoute(searchText)
    }
    
    //MARK : - TableView Delegates
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
            if let data = dataSource{
                cell.textLabel!.text = data[(indexPath as NSIndexPath).row].name
            }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let data = dataSource{
            selectedDelegate?.selectedLocation(data[(indexPath as NSIndexPath).row])
            navigationController?.popToRootViewController(animated: true)
        }
    }
    //MARK : - TableView DataSources
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if let data = dataSource{
            return data.count
        }
        return 0
    }

}
