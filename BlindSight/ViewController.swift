//
//  ViewController.swift
//  BlindSight
//
//  Created by Lucas Mac-Pro on 7/27/16.
//  Copyright © 2016 Lucas Mac-Pro. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import CoreBluetooth
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


class ViewController: UIViewController,MKMapViewDelegate,CLLocationManagerDelegate,SelectLocation,CBPeripheralDelegate,CBCentralManagerDelegate,CBPeripheralManagerDelegate{

    //MARK: - OUTLETS
    @IBOutlet var mapItem: MKMapView!{
        didSet{
            mapItem.delegate = self
            mapItem.showsUserLocation = true
            mapItem.setCenter(mapItem.userLocation.coordinate, animated: true)
        }
    }

    @IBOutlet var stateLabel: UILabel!

    //MARK: - VARIABLES DELEGATES

    var locationManager : CLLocationManager!{
        didSet{
            locationManager.requestWhenInUseAuthorization()
            locationManager.desiredAccuracy = CLLocationAccuracy(exactly: 1.0)!
            locationManager.delegate = self
        }
    }

    var centralManager : CBCentralManager!
    
    var myPeripheral : CBPeripheral!

    var characteristics: [CBCharacteristic]?

    var myBTManager : CBPeripheralManager!

    var shouldZoomMap = false

    var steps : [MKRouteStep]? {
        didSet{
            if mapItem != nil{
                if let stps = steps{
                    self.addPoints(stps)
                }
            }
        }
    }
    
    //MARK: - LIFECYCLE DELEGATES
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        scanPeripheral()
        
    }
    
    //MARK: - Bluetooth methods
    
    @IBAction func connectBluetooth(_ sender: UIBarButtonItem) {
        scanPeripheral()
    }
    
    func scanPeripheral() {
        myBTManager = CBPeripheralManager(delegate: self, queue: nil, options: nil)
        if centralManager == nil {
            centralManager = CBCentralManager(delegate: self, queue: nil)
        }
        centralManager.scanForPeripherals(withServices: nil, options: nil)
        
    }

    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        debugPrint("\(#line) \(#function)")
        debugPrint("checking state")
        
        if central.state != .poweredOn {
            // In a real app, you'd deal with all the states correctly
            return
        }
        central.scanForPeripherals(withServices: nil,options:nil)
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
       
        if let localName = advertisementData[CBAdvertisementDataLocalNameKey] as? String{
            debugPrint("Discovered\(peripheral.name!)")
            
            if localName == "CC41-A" {
                centralManager.stopScan()
                myPeripheral = peripheral
                myPeripheral.delegate = self
                centralManager.connect(myPeripheral, options: nil)
                stateLabel.text = "State = Connected"
            }
        }
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        if peripheral == myPeripheral {
            myPeripheral.discoverServices(nil)
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        if peripheral == myPeripheral{
            if let services = myPeripheral.services{
                for serv in services{
                    myPeripheral.discoverCharacteristics(nil, for: serv)
                }
            }
        }
    }
    
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error:Error?) {
        debugPrint("Found \(service.characteristics?.count) characteristics: \(service.characteristics)")
        if peripheral == myPeripheral{
            if service.characteristics?.count == 1{
                self.characteristics = service.characteristics
            }
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        if peripheral == myPeripheral{
            stateLabel.text = "State = Disconnected"
        }
    }

    func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager) {
        if peripheral.state == .poweredOff {
            stateLabel.text = "State = Disconnected"
        }
    }
    //MARK: - GENERAL METHODS
    
    func requestUserLocation() {
        locationManager = CLLocationManager()
    }
    
    func setupView() {
        requestUserLocation()
    }
    
    func whatShouldHappen(_ step : MKRouteStep) {
        if isCloseToAPoint(step){
            if let index = getIndexOfStep(step){
                if let list = steps{
                    if list.contains(step){
                        steps?.remove(at: index)
                    }
                }
            }
            if steps?.count != 0 {
                makeItVibrate(step)
            }else if steps?.count == 0{
                routeHasEnded(step)
            }
        }
    }
    
    func routeHasEnded(_ step : MKRouteStep) {
        if myPeripheral.state == .connected{
            let data = "3".data(using: String.Encoding.utf8)
            if let chars = characteristics{
                for characteristic in chars{
                    debugPrint("sending 3 to turn show route has ended")
                    myPeripheral.writeValue(data!, for: characteristic,type: CBCharacteristicWriteType.withoutResponse)
             
                    let alert = UIAlertController(title: "Atenção", message: "Você chegou no seu destino.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)

                }
            }
        }else{
            debugPrint("not connected")
        }
    }

    
    func getIndexOfStep(_ step : MKRouteStep)-> Int? {
        if let steps  = steps{
            for i in 0...steps.count{
                if steps[i] == step{
                    return i
                }
            }
        }
        return nil
    }
    
    func isCloseToAPoint(_ step : MKRouteStep) -> Bool {
        if let map = mapItem{
            let distance = map.userLocation.location?.distance(from: CLLocation(latitude: step.polyline.coordinate.latitude, longitude: step.polyline.coordinate.longitude))
            
            if distance < 10 {
                return true
            }
        }
        
        return false
    }
    
    func makeItVibrate(_ step : MKRouteStep) {
        if step.instructions.contains("left"){
            vibrateLeft()
            return
        }else if step.instructions.contains("right"){
            vibrateRight()
            return
        }
        goAhead()
    }
    
    func goAhead() {
        // tell user to go straigth ahead
    }
    
    func vibrateLeft() {
        if myPeripheral.state == .connected{
            let data = "0".data(using: String.Encoding.utf8)
            if let chars = characteristics{
                for characteristic in chars{
                    debugPrint("sending 0 to turn off")
                    myPeripheral.writeValue(data!, for: characteristic,type: CBCharacteristicWriteType.withoutResponse)
                }
            }
        }else{
            debugPrint("not connected")
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        debugPrint(error)
    }
    func vibrateRight() {
        if myPeripheral.state == .connected{
            let data = "2".data(using: String.Encoding.utf8)
            if let chars = characteristics{
                for characteristic in chars{
                    debugPrint("sending 2 to turn on")
                    myPeripheral.writeValue(data!, for: characteristic,type: CBCharacteristicWriteType.withoutResponse)
                }
            }
        }else{
            debugPrint("not connected")
        }
    }
    
    //MARK: - MAPS DELEGATES
    
    func selectedLocation(_ placemark: MKMapItem?) {
        if let place = placemark{
            title = place.name
            requestRoute(place)
        }
    }
    
    func requestRoute(_ item : MKMapItem) {
        let request = MKDirectionsRequest()
        request.source =  MKMapItem(placemark: MKPlacemark(coordinate: mapItem.userLocation.coordinate, addressDictionary: nil))
        request.destination = item
        request.requestsAlternateRoutes = false
        request.transportType = .walking
        
        let directions = MKDirections(request: request)
        
        directions.calculate { [unowned self] response, error in
            guard let res = response else { return }
            self.steps = res.routes.first?.steps
            for route in res.routes {
                self.mapItem.add(route.polyline)
                self.mapItem.setVisibleMapRect(route.polyline.boundingMapRect, animated: true)
            }
        }
    }
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        if shouldZoomMap == false{
            if #available(iOS 9.0, *) {
                mapItem.setCamera(MKMapCamera(lookingAtCenter: mapItem.userLocation.coordinate, fromDistance: 500, pitch: 30, heading: 0), animated: true)
                shouldZoomMap = true
            } else {
                // Fallback on earlier versions
            }
        }
        if let point = steps?.first{
            whatShouldHappen(point)
        }
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(polyline: overlay as! MKPolyline)
        renderer.strokeColor = UIColor.blue
        return renderer
    }
    
    
    func addPoints(_ points : [MKRouteStep]) {
        self.mapItem.addAnnotations(points.map{
            let annotation = MKPointAnnotation()
            annotation.coordinate = $0.polyline.coordinate
            annotation.title = $0.instructions
            return annotation
            }
        )
    }
    
    //MARK: - RIGHT LEFT TEST BUTTONS
    
    @IBAction func rightButtonAction(_ sender: UIButton) {
        vibrateRight()
    }
    
    @IBAction func leftButtonAction(_ sender: UIButton) {
        vibrateLeft()
    }
    
    //MARK: - Prepare for Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let next = segue.destination as? SearchViewController{
            next.selectedDelegate = self
        }
    }
}
